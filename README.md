# PnP Provisioning Basic Sample

## Install SharePoint PnP PowerShell

```bash
powershell ./install.ps1
```

## Provisioning Templates

Use provisioning template which corresponds to [Provisioning Schema](https://github.com/SharePoint/PnP-Provisioning-Schema).

## Apply Provisioning Template

```bash
powershell ./deploy.ps1 -SiteURL "https://contoso.sharepoint.com/sites/test" -Template "./template.xml"
```