[CmdletBinding()]
Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$SiteURL,

  [Parameter(Mandatory=$False,Position=2)]
  [string]$Template = "$PSScriptRoot\template.xml",

  [Parameter(Mandatory=$False,Position=2)]
  [string]$Handlers = "All"
);

Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass -Force;

Set-PnPTraceLog -on -level Debug;

$StartTime = Get-Date;

$Connection = Connect-PnPOnline -Url $SiteURL -ReturnConnection -UseWebLogin;

Write-Host "Applying template to $($SiteURL)";

Apply-PnPProvisioningTemplate `
  -Path $Template `
  -ProvisionContentTypesToSubWebs:$True `
  -ProvisionFieldsToSubWebs:$True `
  -Connection $Connection `
  -Handlers $Handlers;

$EndTime = Get-Date;
$TimeSpan = New-TimeSpan $StartTime $EndTime;
Write-Host "Execution time: $timespan";
