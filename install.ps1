$PnPModuleName = "SharePointPnPPowerShellOnline";

Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass -Force;
Install-Module -Name $PnPModuleName -Scope CurrentUser -Force;
Import-Module -Name $PnPModuleName -DisableNameChecking;